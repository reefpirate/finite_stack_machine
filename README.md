# Finite Stack Machine - 
A finite state machine variant that uses a stack of custom states to drive the behavior of 
virtually any entity in Godot.

There's a fair amount of comments in the code, and overall there is not a lot of code so hopefully
you can make some sense of it. I will return to do a more thorough write-up once I've tested and
implemented a few more features.

If you were to use this StackMachine in your project you only really need finite_stack_machine.gd,
globals.gd and at least one State script. You can take st_blank.gd and use that as a template for
more states. (globals.gd needs to be added as an AutoLoad singleton named Globals)

Basically you can create and add a StackMachine to any Godot entity, which can process state scripts that
you can write yourself. Many possible applications, some of which I hope I demonstrated in the little
demo game.

I don't really know what I'm doing with repo permissions, so I've limited them to project members for
editing the repo. I don't know what is standard in these things. But please feel free to download, copy,
paste do whatever you want with the code. Hopefully to improve it! I would appreciate knowing of any
cool developments, hacks or tweaks that I could use to improve my system.

And sorry, I messed up the repo's ignore settings. I'll fix this soon.

I found this today: https://en.wikipedia.org/wiki/Pushdown_automaton Seems relevant :P

```gdscript
onready var string = "Hello, world."
```