extends Node

const STACK_MACHINE = preload("res://addons/finite_stack_machine/finite_stack_machine.gd")

const ST_IDLE = preload("res://demo/states/st_idle.gd")
const ST_WALK = preload("res://demo/states/st_walk.gd")
const ST_RUN = preload("res://demo/states/st_run.gd")
const ST_JUMP = preload("res://demo/states/st_jump.gd")
const ST_OUCH = preload("res://demo/states/st_ouch.gd")
