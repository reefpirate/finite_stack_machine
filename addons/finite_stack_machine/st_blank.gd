# State - Blank Template
extends "res://addons/finite_stack_machine/finite_stack_machine.gd".State

# minion = Entity using the state
# state_time = time in seconds we have been in this state

# Have process return 1 or -1 when the state is finished,
# otherwise return 0 to continue

# Constructor, called on state creation
func _init():
	name = "Blank State"

# on_start is run once when the Command is first processed (input or process)
func on_start(): # Run once when the state starts
	pass

# on_end is called once when the state is finished or 'popped' from the stack 
func on_end(): # Run once when the state is finished
	pass

# typically process is called once every frame in the minion's _process() callback
func process(delta): # Run usually each step of the minion, but can be called to run whenever
	pass

# typically input is called in the minion's _input() callback
func input(event):
	pass