# FiniteStackMachine.gd

var minion
# Entity that the stateStack is attached to

var stack_machine = []
# This member array is the heart of the whole operation.
# It looks like this:
# [[state_object1, [arg1, arg2, ...]], [state_object2, [arg1, arg2, ...]], ...]

var default_state
# a place to store a fallback state

signal state_time_changed(new_value)
signal stack_machine_changed()

func process(delta):
	# Method for processing the current state @ pos 0
	if stack_machine.size() > 0:
		# get top state info
		var state_object = stack_machine[0]
		
		# Check for first time running, and start state
		if state_object.started == false:
			state_object.on_start()
			state_object.started = true
			
		# Run the state's process method
		state_object.process(delta)
		
		# increment state_time by delta
		state_object.state_time += delta
		emit_signal("state_time_changed", state_object.state_time)
		
	elif default_state != null:
		# If state stack is empty, we push the default
		push_create(default_state.state_class, default_state.args)
		process(delta) # and try to run it right away

# input() needs more testing!
func input(event):
	# Method for processing the current state @ pos 0
	if stack_machine.size() > 0:
		# get top state info
		var state_object = stack_machine[0]
		
		# Check for first time running, and start state
		if state_object.started == false:
			state_object.on_start()
			state_object.started = true
			
		# Run the state's input method
		state_object.input(event)
		
	elif default_state != null:
		# If state stack is empty, we push the default
		push_create(default_state.state_class, default_state.args)
		input(event) # and try to run it right away

func push(state):
	# add a state to front of state stack
	stack_machine.push_front(state)
	emit_signal("stack_machine_changed")

func add(state):
	# add a state to end of state stack
	stack_machine.append(state)
	emit_signal("stack_machine_changed")

func push_create(state_class, args = []):
	# create and add a state class with args to front of state stack
	push(state_create(state_class, args))

func add_create(state_class, args = []):
	# create and add a state class and args to end of state stack
	add(state_create(state_class, args))

func pop():
	# pop the top state from the from state stack
	var state_object = stack_machine.pop_front()
	# process on_end event for the popped state
	state_object.on_end()
	emit_signal("stack_machine_changed")

func state_create(state_class, args = []):
	# creates an entry appropriate for the stack_machine array
	# [state_ref, [arg1, arg2, ...]]
	var state_object = state_class.new()
	state_object.minion = minion
	state_object.state_class = state_class
	state_object.args = args
	
	return state_object

class State:
	var name = "state Superclass"
	var minion
	var state_class
	var args = []
	var state_time = 0
	var started = false
	
	func on_start():
		pass
	
	func on_end():
		pass
	
	func process(delta):
		pass
	
	func input(event):
		pass