# Command - Blank Template
extends "res://addons/finite_stack_machine/finite_stack_machine.gd".State

# minion = Entity using the Command
# state_time = time in seconds we have been in this state

# Have process return 1 or -1 when the command is finished,
# otherwise return 0 to continue

func _init():
	name = "IDLE"

func on_start(): # Run once when the command starts
	pass
	
func on_end(): # Run once when the command is finished
	pass

func process(delta): # Run usually each step of the minion, but can be called to run whenever
	# return 0 to continue in this state
	# return 1 to end state, return -1 to end state and process next state right away
	if Input.is_action_just_pressed("ui_up") == true:
		minion.stack_machine.pop()
		minion.stack_machine.push_create(Globals.ST_WALK, ["Arg1", "Arg2", "etc."])

func input(event):
	pass