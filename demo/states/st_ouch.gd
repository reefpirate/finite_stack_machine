# state - Blank Template
extends "res://addons/finite_stack_machine/finite_stack_machine.gd".State

# minion = Entity using the state
# state_time = time in seconds we have been in this state

# Have process return 1 or -1 when the state is finished,
# otherwise return 0 to continue

func _init():
	name = "Ouch."

func on_start(): # Run once when the command starts
	pass
	
func on_end(args = []): # Run once when the state is finished
	pass

func process(delta): # Run usually each step of the minion, but can be called to run whenever
	# return 0 to continue in this state
	# return 1 to end state, return -1 to end state and process next state right away
	if state_time > 3:
		minion.stack_machine.pop()

func input(event):
	pass