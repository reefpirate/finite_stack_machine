extends CenterContainer

onready var state_time_counter = find_node("StateTimeCounter")
onready var state_container = $HBox/StackMachine/States

onready var stack_machine = Globals.STACK_MACHINE.new()

func _ready():
	stack_machine.minion = self
	stack_machine.default_state = stack_machine.state_create(Globals.ST_IDLE)
	
	stack_machine.connect("state_time_changed", self, "_on_state_time_changed")
	stack_machine.connect("stack_machine_changed", self, "_on_stack_machine_changed")

func _process(delta):
	stack_machine.process(delta)

func _input(event):
	stack_machine.input(event)

func _on_state_time_changed(new_value):
	state_time_counter.text = str(new_value)

func _on_stack_machine_changed():
	var state_labels = state_container.get_children()
	for l in state_labels:
		l.queue_free()
	for c in stack_machine.stack_machine:
		var label = Label.new()
		state_container.add_child(label)
		label.text = str(c.name, " - ", c.args)
